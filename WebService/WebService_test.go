package WebService_test

import (
	"fmt"
	"gitlab.com/bellbainpup/toolbox/WebService"
)

func Example_WebService() {
	url := "/myurl"

	ws := WebService.NewWebService(url, &WebService.StubDbInfo{}, &WebService.People{})

	go ws.Start()
	go WebService.Listen(3050)

	resp := WebService.Get("http://localhost:3050/myurl")

	fmt.Print(resp)

	// Output:
	// [{"name":"Bob","age":"35"},{"name":"TIM","age":"2"},{"name":"JAKE","age":"25"}]
}

func Example_WebServices_dog() {
	url := "/myurl3"

	ws := WebService.NewWebService(url, &WebService.DogDbInfo0{}, &WebService.Dogs{})

	go ws.Start()
	go WebService.Listen(3050)

	resp := WebService.Get("http://localhost:3050/myurl3")

	fmt.Print(resp)

	// Output:
	// [{"name":"rex","age":"1","owner":"ted"},{"name":"jet","age":"2","owner":"ted"},{"name":"max","age":"3","owner":"ted"}]
}


func Example_WebService_Paramas() {


	url := "/url1"

	ws := WebService.NewWebService(url, &WebService.DogDbInfo{}, &WebService.Dogs{})

	go ws.Start()
	go WebService.Listen(3050)

	resp := WebService.Get("http://localhost:3050/url1?owner=ted")

	fmt.Print(resp)

	// Output:
	// [{"name":"rex","age":"1","owner":"ted"},{"name":"jet","age":"2","owner":"ted"},{"name":"max","age":"3","owner":"ted"}]
}

func Example_WebService_Paramas_x2() {


	url := "/url2"

	ws := WebService.NewWebService(url, &WebService.DogDbInfo2{}, &WebService.Dogs{})

	go ws.Start()
	go WebService.Listen(3050)

	resp := WebService.Get("http://localhost:3050/url2?owner=ted&name=rex")

	fmt.Print(resp)

	// Output:
	// [{"name":"rex","age":"1","owner":"ted"}]
}



