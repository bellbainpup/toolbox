package WebService

import (
	"log"
	"net/http"
	"strconv"
)

type Webserverer interface {
	AddWebService()
	Listen()
}

type WebServer struct {
	port int
	webServices []WebServicer
}

func NewWebServer(port int) *WebServer {
	return &WebServer{port: port}
}

func (w *WebServer) AddWebService(webservicer WebServicer) {
	w.webServices = append(w.webServices, webservicer)
}

func (w *WebServer) start()  {
	for _, service := range w.webServices {
		go service.Start()
	}
}

func (w *WebServer) StartAndListen() {

	w.start()
	go w.listen()
}

func (w *WebServer) listen() {
	portStr := ":" + strconv.Itoa(w.port)
	if err := http.ListenAndServe(portStr, nil); err != nil {
		log.Fatal(err)
	}
}

