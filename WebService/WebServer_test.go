package WebService_test

import (
	"fmt"
	"gitlab.com/bellbainpup/toolbox/WebService"
)

//

func Example_WebServer() {

	ws := WebService.NewWebService("/myurl", &WebService.StubDbInfo{}, &WebService.People{})



	wsvr := WebService.NewWebServer(3050)
	wsvr.AddWebService(ws)
	wsvr.StartAndListen()

	resp := WebService.Get("http://localhost:3050/myurl")

	fmt.Print(resp)
    // Output:
    // [{"name":"Bob","age":"35"},{"name":"TIM","age":"2"},{"name":"JAKE","age":"25"}]
}



