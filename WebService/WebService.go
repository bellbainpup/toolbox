package WebService

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

type WebServicer interface {
	Start()
}

type Scanner interface {
	Scan(rows *sql.Rows)
}

type WebService struct {
	url      string
	database Databaser
	scanner  Scanner
}

func NewWebService(url string, database Databaser, scanner Scanner) *WebService {
	return &WebService{url: url, database: database, scanner: scanner}
}
func (ws *WebService) Start() {

	go http.HandleFunc(ws.url, ws.action())

	// go Listen(port)

}

func Listen(port int) {
	portStr := ":" + strconv.Itoa(port)
	if err := http.ListenAndServe(portStr, nil); err != nil {
		log.Fatal(err)
	}
}
func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func (ws *WebService) action() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		dbInfo := ws.database

		db := dbInfo.GetDB()
		defer db.Close()

		query := dbInfo.Query()

		//------------------------------------------------------------------------------
		var iparams []interface{}
		params := Params(r)
		for _, param := range params {
			iparams = append(iparams, param)
		}
		//------------------------------------------------------------------------------

		rows, err := db.Query(query, iparams...)




		defer rows.Close()
		ws.scanner.Scan(rows)

		if err != nil {
			log.Fatal(err)
		}

		err = json.NewEncoder(w).Encode(ws.scanner)

		if err != nil {
			fmt.Println(err)
		}
	}
}
