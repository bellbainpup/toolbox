package TestPackage

import (
	"gitlab.com/bellbainpup/toolbox/Logger"
	"testing"
)

func TestLogger(t *testing.T) {

	log := Logger.GetLogger("log.json")

	c := make(chan bool)

	go func(c chan bool) {
		log.Info("My info")
		log.Errorf("this is an error %s", "---------")
		c <- true
	}(c)

	go func(c chan bool) {
		log.Info("My info")
		log.Errorf("this is an error %s", "---------")
		c <- true
	}(c)

	go func(c chan bool) {
		log.Info("My info")
		log.Errorf("this is an error %s", "---------")
		c <- true
	}(c)

	<-c
}
