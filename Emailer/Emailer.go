package Emailer

import (
	"encoding/xml"
	"fmt"
	"gitlab.com/bellbainpup/toolbox/Fd"
	"gitlab.com/bellbainpup/toolbox/XmlCfg"
	"log"
	"net/smtp"
	"os"
	"path/filepath"
	"strconv"
)

type Emailer struct {
	From     string `xml:"from"`
	To       string `xml:"to"`
	Password string `xml:"password"`
	Host     string `xml:"host"`
	Port     int    `xml:"port"`
}

var dir = os.Getenv("GO_XML_CFG")
var dir2 = filepath.Join(dir, "emailer")
var fp = filepath.Join(dir2, "emailer.xml")

func GenerateConfig(host string, port int) {
	em := &Emailer{Host: host, Port: port}

	Fd.MakeDir(dir2)

	XmlCfg.ToXml(em, fp)
}

func FromXml(fp string) *Emailer {
	data, err := Fd.ReadBytesFromFile(fp)
	var i *Emailer
	err = xml.Unmarshal(data, &i)
	if err != nil {
		fmt.Printf("error: %v", err)
		return i
	}
	return i
}

func Send(body string) {

	i := FromXml(fp)

	em := i
	from := em.From
	pass := em.Password
	to := em.To

	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: Hello there\n\n" +
		body

	fulladdress := em.Host + ":" + strconv.Itoa(em.Port)
	err := smtp.SendMail(fulladdress,
		smtp.PlainAuth("", from, pass, em.Host),
		from, []string{to}, []byte(msg))

	if err != nil {
		log.Printf("smtp error: %s", err)
		return
	}

}
