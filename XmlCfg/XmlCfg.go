package XmlCfg

import (
	"encoding/xml"
	"fmt"
	"gitlab.com/bellbainpup/toolbox/Fd"
)

func ToXml(i interface{}, fp string) []byte {
	output, err := xml.MarshalIndent(i, "", "    ")
	if err != nil {
		fmt.Printf("error: %v\n", err)
	}

	Fd.WriteBytesToNewFile(fp, output)

	return output
}

func FromXml(i interface{}, fp string) interface{} {

	b, err := Fd.ReadBytesFromFile(fp)

	if err != nil {
		panic(err)
	}

	if err := xml.Unmarshal(b, &i); err != nil {
		panic(err)
	}

	return i
}

func FromXmlToSliceBytes(i interface{}, fp string) []byte {

	b, err := Fd.ReadBytesFromFile(fp)

	if err != nil {
		panic(err)
	}

	return b
}
