package XmlCfg_test

import (
	"fmt"
	"gitlab.com/bellbainpup/toolbox/XmlCfg"
	"os"
)

type Person struct {
	Name string `xml:"name"`
	Age  int    `xml:"age"`
}

func NewPerson(name string, age int) *Person {
	return &Person{Name: name, Age: age}
}

func ExampleToXml() {
	data := XmlCfg.ToXml(NewPerson("dave", 40), "./MyFile.xml")
	os.Stdout.Write(data)
	// Output:
	//<Person>
	//	<name>dave</name>
	// 	<age>40</age>
	//</Person>
}

func ExampleFromXml() {
	p := Person{}

	data := XmlCfg.FromXml(&p, "./MyFile.xml")

	s := data.(*Person).Name
	fmt.Println(s)
	// Output: dave
}
