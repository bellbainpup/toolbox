package Updater

import (
	"encoding/xml"
	"fmt"
	"github.com/fatih/color"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type AutoUpdateConfig struct {
	Command    string `xml:Command`
	ExePath    string `xml:ExePath`
	SourcePath string `xml:SourcePath`
	BackupPath string `xml:BackupPath`
}

func Activate() interface{} {

	return os.Getpid()

	a := NewAutoUpdateConfig()
	fullpath := filepath.Join(a.ExePath, "Updater.cfg")
	// if file does not exist
	if _, err := os.Stat(fullpath); os.IsNotExist(err) {
		a.ToXml(fullpath)
	}
	// if file exists
	if _, err := os.Stat(fullpath); err == nil {
		a.FromXml(fullpath)
	}
	SetupGitSsh("c:/sshkeys/id_rsa", "c:\\DEV\\outsideprj")
	DirCommand("git", a.SourcePath, "pull", "--tags")
	tags := a.GetTags(a.SourcePath)

	svSlice := GenerateSliceOfSemanticVersions(tags)

	highestSemVer := svSlice.GetHighestSemVer()
	fmt.Print(highestSemVer.String())
	fmt.Println("--------------------------")
	strCurrentTag := SimpleCommand(a.SourcePath, "git", "describe", "--tags")
	fmt.Println("_______________________________________________________________")
	fmt.Println(a.SourcePath)
	fmt.Println("_______________________________________________________________")
	currentSemVer := GetSemanticVersionFromString(strCurrentTag)

	if highestSemVer.IsGreaterThan(currentSemVer) {
		color.Red("There is a new version avalible ( %s ) would you like to install", highestSemVer.String())

	}

	isConfirmed := Ask4confirm()
	if isConfirmed {
		fmt.Print("yes")
	} else {
		fmt.Println("not confirmed")
	}

	slice := []string{currentSemVer.String(), highestSemVer.String()}

	duo := GenerateSliceOfSemanticVersions(slice)

	highest := duo.GetHighestSemVer()

	DirCommand("git", "checkout", highest.String())
	return nil
}

func NewAutoUpdateConfig() *AutoUpdateConfig {

	getwd, err := os.Getwd()

	if err != nil {
		fmt.Println(err)
	}
	return &AutoUpdateConfig{Command: filepath.Base(os.Args[0]), ExePath: getwd, SourcePath: getwd, BackupPath: filepath.Join(getwd, "\\Backup")}
}

func (a *AutoUpdateConfig) ToXml(fullpath string) {

	// if file does not exist
	if _, err := os.Stat(fullpath); os.IsNotExist(err) {
		createXmlFile(a, fullpath)
	}
}

func createXmlFile(data *AutoUpdateConfig, fullpath string) {
	bytes, err := xml.MarshalIndent(data, "  ", "    ")
	check(err)
	err = ioutil.WriteFile(fullpath, bytes, 0644)
	check(err)
}

func (a *AutoUpdateConfig) FromXml(fp string) error {
	//read file to []bytes
	b, err := ioutil.ReadFile(fp)
	check(err)
	//unmarshal to xml
	err = xml.Unmarshal(b, &a)
	check(err)
	return nil
}

func SimpleCommand(dir, start string, args ...string) (result string) {
	cmd := exec.Command(start, args...)

	cmd.Dir = dir
	bytes, err := cmd.Output()
	if err != nil {
		fmt.Print(err)
	}
	s := string(bytes)

	return s

}

func BuildApp(sourceCodeDir string, FullPathToNewExe string, sourcefile string) string {
	return SimpleCommand(sourceCodeDir, "go", "build", "-o", FullPathToNewExe, sourcefile)
}

func ListCommand(dir, start string, args ...string) []string {
	cmd := exec.Command(start, args...)

	cmd.Dir = dir
	bytes, err := cmd.Output()
	if err != nil {
		fmt.Print(err)
	}
	s := string(bytes)

	// list of tags in string format
	list := strings.Split(s, "\n")

	//empty list
	outList := []string{}

	// foreach value in list
	for _, value := range list {
		lv := len(value)
		if lv != 0 {
			if []rune(value)[0] == 'v' {

			}

			outList = append(outList, value)
		} else {
			//return nil
		}
	}

	return outList

}

func (a AutoUpdateConfig) GetTags(sourceCodeDir string) []string {
	return ListCommand(sourceCodeDir, "git", "tag", "-l")
}

type SemanticVersion struct {
	Major int
	Minor int
	Patch int
}
type SemVerCollection []*SemanticVersion

func IsValidForSemanticVersion(s string) bool {

	split := strings.Split(s, "v")
	var vsplit string

	if len(split) > 1 {
		vsplit = split[1]
	}
	dotSplit := strings.Split(vsplit, ".")

	for _, val := range dotSplit {
		if !IsNumber(val) {
			return false
		}
	}

	return true
}

func IsNumber(s string) bool {
	if _, err := strconv.Atoi(s); err == nil {
		return true
	} else {
		return false
	}
}
func GetSemanticVersionFromString(v string) *SemanticVersion {
	withoutv := strings.Split(v, "v")[1]

	split := strings.Split(withoutv, ".")

	major, err := strconv.Atoi(split[0])
	minor, err := strconv.Atoi(split[1])
	patch, err := strconv.Atoi(split[2])

	if err != nil {
		fmt.Print(err)
	}

	sm := &SemanticVersion{
		Major: major,
		Minor: minor,
		Patch: patch,
	}

	return sm
}
func GenerateSliceOfSemanticVersions(input []string) SemVerCollection {

	slice := []*SemanticVersion{}

	for _, value := range input {
		if !IsValidForSemanticVersion(value) {
			fmt.Printf("Incompatable Tag found: %s  you should removen this from the repo.", value)
			fmt.Println()
		} else {
			slice = append(slice, GetSemanticVersionFromString(value))
		}
	}

	return slice

}

func (sv *SemanticVersion) IsGreaterThan(next *SemanticVersion) bool {
	if sv.Major > next.Major {
		return true
	} else if sv.Major == next.Major && sv.Minor > next.Minor {
		return true
	} else if sv.Major == next.Major && sv.Minor == next.Minor && sv.Patch > next.Patch {
		return true
	} else {
		return false
	}
}
func (sv SemanticVersion) String() string {
	sb := strings.Builder{}
	sb.WriteString("v")
	sb.WriteString(strconv.Itoa(sv.Major))
	sb.WriteString(".")
	sb.WriteString(strconv.Itoa(sv.Minor))
	sb.WriteString(".")
	sb.WriteString(strconv.Itoa(sv.Patch))

	return sb.String()
}
func (svc SemVerCollection) GetHighestSemVer() *SemanticVersion {
	var n = len(svc)
	for i := 1; i < n; i++ {
		j := i
		for j > 0 {

			if svc[j-1].IsGreaterThan(svc[j]) {
				svc[j-1], svc[j] = svc[j], svc[j-1]
			}
			j = j - 1
		}
	}
	asd := svc
	fmt.Print(asd)
	return svc[len(svc)-1]
}
func SetupGitSsh(keydir, sourcedir string) {
	sshKeysDir := keydir
	sshloc := fmt.Sprintf("ssh -i %s", sshKeysDir)
	sorceCodeFolder := sourcedir
	DirCommand("git", sorceCodeFolder, "config", "core.sshCommand", sshloc)
}

func DirCommand(app string, dir string, args ...string) string {

	cmd := exec.Command(app, args...)
	cmd.Dir = dir
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Println(err.Error())
		return "error"
	}

	s := string(stdout)

	return s
}

func KillProcess(pid int) {

	process, err := os.FindProcess(pid)

	if err != nil {

	}

	process.Kill()
}
func Ask4confirm() bool {
	var s string

	fmt.Printf("(y/N): ")
	_, err := fmt.Scan(&s)
	if err != nil {
		panic(err)
	}

	s = strings.TrimSpace(s)
	s = strings.ToLower(s)

	if s == "y" || s == "yes" {
		return true
	}
	return false
}
func CheckForUpdates() {

}
