package Updater

import (
	"fmt"
	"gitlab.com/bellbainpup/toolbox/Fd"
	"path/filepath"
	"testing"
)

//cd existing_folder
//git init
//git remote add origin git@gitlab.com:dafabe/utr.git
//git add .
//git commit -m "Initial commit"
//git push -u origin master

func TestUpdaterActivateEndToEnd(t *testing.T) {

	// Setup repo in loc A

	rootDir := "C:\\TEST_DIR"
	locADir := filepath.Join(rootDir, "loc_A")
	locBDir := filepath.Join(rootDir, "loc_B")

	Fd.RemDir(rootDir)
	Fd.MakeDir(rootDir)
	Fd.MakeDir(locADir)
	//Fd.MakeDir(locBDir)
	//time.Sleep(5 * time.Second)

	fmt.Println(DirCommand("git", locADir, "init"))
	SetupGitSsh("c:/sshkeys/id_rsa", locADir)
	Fd.CreateFile(filepath.Join(locADir, "MyFile.txt"))

	Fd.CopyDir(locADir, locBDir)
	/*
		fmt.Println(DirCommand("git", locADir, "remote", "add", "origin", "git@gitlab.com:dafabe/utr.git"))
		fmt.Println(DirCommand("git", locADir, "add", "."))
		fmt.Println(DirCommand("git", locADir, "commit", "-m", "Initial commit"))
		fmt.Println(DirCommand("git", locADir, "push",  "-u", "origin", "master"))
	*/
	want := ""

	got := ""

	if want != got {
		t.Errorf("Expected: %s  Acutal:   %s", want, got)
	}

}
