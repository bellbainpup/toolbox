package EHand

import (
	"errors"
	"fmt"

	"gitlab.com/bellbainpup/toolbox/EHand/Break/Both"
)

func ExampleBreak() {

	Break.Both(errors.New("new error"))
	fmt.Println("")
	// Output: dfbgdf
}
