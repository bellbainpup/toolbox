package Excel_test

import (
	"gitlab.com/bellbainpup/toolbox/Excel"
	"testing"
)

func TestSomsi_ToXlsx(t *testing.T) {

	somsi := Excel.Somsi{}

	somsi.Add(map[string]interface{}{"Name": "Sue", "Age": 22})
	somsi.Add(map[string]interface{}{"Name": "Bob", "Age": 35})
	somsi.Add(map[string]interface{}{"Name": "Bill", "Age": 50})

	//for name, tc := range tests {
	t.Run("", func(t *testing.T) {
		xlsx := somsi.ToXlsx("./myfile.xlsx", []string{"Name", "Age"}, Excel.NewCellRef("A", 1))
		cellToCheck := Excel.NewCellRef("A", 1).String()
		got := xlsx.GetCellValue("Sheet1", cellToCheck)
		want := "Name"
		if got != want {
			t.Errorf("want: %s, got: %s", want, got)
		}
	})
	//}
}
func TestSomsi_SortSomsi(t *testing.T) {

	somsi := Excel.Somsi{}

	somsi.Add(map[string]interface{}{"Type": "Reprint", "Cost": "22.30"})
	somsi.Add(map[string]interface{}{"Type": "New", "Cost": "35.10"})
	somsi.Add(map[string]interface{}{"Type": "Reprint", "Cost": "50.11"})
	somsi.Add(map[string]interface{}{"Type": "New", "Cost": "22.30"})
	somsi.Add(map[string]interface{}{"Type": "Reprint", "Cost": "35.10"})
	somsi.Add(map[string]interface{}{"Type": "New", "Cost": "50.11"})

	//for name, tc := range tests {
	t.Run("", func(t *testing.T) {
		//xlsx := somsi.ToXlsx("./myfile.xlsx", []string{"Name", "Age"}, Excel.NewCellRef("A", 1))
		// cellToCheck := Excel.NewCellRef("A", 1).String()
		got := SortSomsi(&somsi, "Type")
		want := &Excel.Somsi{}
		if got != want {
			t.Errorf("want: %s, got: %s", want, got)
		}
	})
	//}
}

func SortSomsi(somsi *Excel.Somsi, s string) *Excel.Somsi {
	return somsi
}
