package Excel

import (
	"fmt"
)

func ExampleCellRef_IncrementCol() {

	cr := NewCellRef("B", 1)

	cr, err := cr.IncrementCol()

	if err != nil {

	}
	fmt.Println(cr.Col)
	// Output: C
}

func ExampleCellRef_IncrementCol_Z() {

	cr := NewCellRef("Z", 1)

	cr, err := cr.IncrementCol()

	if err != nil {
		fmt.Println(err)
	}

	// Output: only 26 cols allowed
}

func ExampleCellRef_IncrementRow() {

	cr := NewCellRef("B", 1)

	cr = cr.IncrementRow()

	fmt.Println(cr.Row)
	// Output: 2
}

func ExampleCellRef_String() {

	cr := NewCellRef("B", 1)

	fmt.Println(cr.String())
	// Output: B1
}
