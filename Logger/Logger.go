package Logger

import (
	"fmt"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	. "os"
)

func GetLogger(logfile string) *logrus.Logger {

	dt := time.Now()
	dts := dt.Format("02-Jan-2006")

	sb := strings.Builder{}

	sb.WriteString(dts)
	sb.WriteString("   ")
	sb.WriteString(logfile)
	fp := sb.String()

	fp = strings.ReplaceAll(fp, ",", " ")
	fp = strings.ReplaceAll(fp, ":", "-")

	var log = logrus.New()
	var file, err = OpenFile(fp, O_RDWR|O_CREATE|O_APPEND, 0666)
	if err != nil {
		fmt.Println("Could Not Open Log File : " + err.Error())
	}
	log.SetOutput(file)
	log.SetFormatter(&logrus.JSONFormatter{})

	return log

}
