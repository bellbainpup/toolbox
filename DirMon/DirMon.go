package DirMon

import (
	"fmt"
	"github.com/fatih/color"
	"github.com/fsnotify/fsnotify"
	"os"
	"path/filepath"
	"time"
)

const Hold = "c:\\fsnotify\\Hold"
const In = "c:\\fsnotify\\In"
const Out = "c:\\fsnotify\\Out"
const Error = "c:\\fsnotify\\Error"

func ProcessFile(fp string) {

	nfp := filepath.Join(Out, filepath.Base(fp))
	err := attemptToMoveFile(fp, nfp)

	if err != nil {
		color.Red(err.Error())
	}

}

func attemptToMoveFile(sourcefp, destfp string) error {

	var tryRename func(attempts int) error

	err := os.Rename(sourcefp, destfp)

	tryRename = func(attempts int) error {

		if err != nil {
			attempts++
			fmt.Print(err)
			if attempts < 1 {
				time.Sleep(1 * time.Millisecond)
				return tryRename(attempts)
			} else {
				return fmt.Errorf("error maximum attempts of %d reached", attempts)
			}
		}

		fmt.Printf("File moved from: %s to: %s", sourcefp, destfp)
		fmt.Println()
		return nil

	}

	if err != nil {
		return tryRename(1)
	}

	fmt.Printf("File moved from: %s to: %s", sourcefp, destfp)
	fmt.Println()
	return nil

}

func Mon(dir string, ext string, f func(fp string) error) {
	fmt.Print("Started.....")
	fmt.Println()
	// creates a new file watcher
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		fmt.Println("ERROR", err)
	}
	defer watcher.Close()

	//
	done := make(chan bool)

	//
	go func() {
		for {
			select {
			// watch for events
			case event := <-watcher.Events:
				t := time.Now()
				dt := t.Format(time.UnixDate)

				if event.Op == 1 && filepath.Ext(event.Name) != ext {
					fmt.Printf("%s FileName: %s Event: %s", dt, event.Name, event.Op)
					fmt.Println()
					err := f(event.Name)
					if err != nil {
						color.Red(err.Error())
					}
				}
				// watch for errors
			case err := <-watcher.Errors:
				fmt.Println("ERROR", err)
			}
		}
	}()

	// out of the box fsnotify can watch a single file, or a single directory
	if err := watcher.Add(dir); err != nil {
		fmt.Println("ERROR", err)
	}

	<-done
}
