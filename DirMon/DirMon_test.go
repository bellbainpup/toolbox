package DirMon

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"testing"
)

func TestDirMonn(t *testing.T) {

	os.MkdirAll(In, os.ModePerm)

	os.MkdirAll(Out, os.ModePerm)

	os.MkdirAll(Error, os.ModePerm)

	c := make(chan bool)

	go Mon(In, ProcessFile)

	go CreateFiles()

	<-c

	fp := filepath.Join(Out, "bigfile1.txt")

	want := false

	got := DoesNotExist(fp)

	if want != got {
		t.Errorf("Expected: %v  Acutal:   %v", want, got)
	}
}

func DoesNotExist(fp string) bool {
	if _, err := os.Stat(fp); os.IsNotExist(err) {
		return true
	}
	return false
}

func CreateFiles() {
	GenFile(750, "bigfile1.txt")
	time.Sleep(5 * time.Second)
	GenFile(300, "bigfile2.txt")
	time.Sleep(10 * time.Second)
	GenFile(10, "bigfile3.txt")
	time.Sleep(5 * time.Second)
	GenFile(40, "bigfile4.txt")
	time.Sleep(5 * time.Second)
	GenFile(100, "bigfile5.txt")
	time.Sleep(5 * time.Second)
	GenFile(500, "bigfile6.txt")
}

func GenFile(i int, s string) {
	bigBuff := make([]byte, i*1000)
	fp := filepath.Join(Hold, s)
	ioutil.WriteFile(fp, bigBuff, 0666)
	np := filepath.Join(In, s)
	os.Rename(fp, np)

}
