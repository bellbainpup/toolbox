package TryThingsOut

import (
	"fmt"
	"testing"
)

func MyFunc(f func(s string) bool) {

	f("")
	f("")
	f("")
	f("")
	f("")
	f("")

}

func TestNamen(t *testing.T) bool {
	i := 0
	f := func(s string) {
		i++
		fmt.Printf("Number: %v --- %s", i, s)
		fmt.Println()
	}

	MyFunc(f)

}
