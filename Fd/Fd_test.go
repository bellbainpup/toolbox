package Fd_test

import (
	"fmt"
	"gitlab.com/bellbainpup/toolbox/Fd"
	"testing"
)

func TestListFilesFps(t *testing.T) {

	t.Run("ListFilesFps", func(t *testing.T) {
		got := Fd.ListFileFps("\\\\server\\public")[0]

		want := ""

		if got != want {
			t.Errorf("Expected %s, got: %s", want, got)
		}
	})

}

func ExampleListFilesFpsDirOnly() {

	got := Fd.ListFileFpsDirsOnly("C:\\PROG_REPS_DEV\\Reps")

	fmt.Print(got)

	// Output: [C:\PROG_REPS_DEV\Reps\ACCOUNTS C:\PROG_REPS_DEV\Reps\PROGRESS]

}

func TestDir_GetActualFiles(t *testing.T) {

}
