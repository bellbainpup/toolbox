package SmartTicker_test

import (
	"fmt"
	"gitlab.com/bellbainpup/toolbox/SmartTicker"
	"testing"
	"time"
)

type MockTime struct {
}

func (m MockTime) Sleep(d time.Duration) {
	time.Sleep(0)
}

func (m MockTime) SleepMin(d time.Duration) {
	time.Sleep(0)
}
func Test(t *testing.T) {

	time := MockTime{}

	cm := func() bool {
		return true
	}

	st := SmartTicker.NewSmartTicker(1, 5, "initial", "subs", func(s string) { fmt.Println(s) }, func(s string) { fmt.Println(s) }, cm)

	st.Start(time)
}
