package Basic_test

import (
	"fmt"
	"gitlab.com/bellbainpup/toolbox/Basic"
	"testing"
)

func TestQueryInfo_QueryToDataGrid(t *testing.T) {

	qi := Basic.QueryInfo{
		ConnectionString: "",
		Query:            "",
		Args:             nil,
	}

	data, err := qi.QueryToDataGrid(Basic.GetDataStub)

	if err != nil {
		fmt.Print(err)
	}
	fmt.Print(data)

}

func TestQueryInfo_QueryToDataGrid_owndata(t *testing.T) {

	qi := Basic.QueryInfo{
		ConnectionString: "",
		Query:            "",
		Args:             nil,
	}

	f := func() ([]map[string]interface{}, error) {

		sm := make([]map[string]interface{}, 3)

		sm = append(sm, map[string]interface{}{"name": "Sue", "age": 25})

		sm = append(sm, map[string]interface{}{"name": "Bob", "age": 55})

		return sm, nil

	}

	data, err := qi.QueryToDataGrid(f)

	if err != nil {
		fmt.Print(err)
	} else {

		fmt.Print(data)
	}

}
